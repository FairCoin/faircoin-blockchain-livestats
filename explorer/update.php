<?php

$BLOCKHEIGHT_REMOTE=0;

function loadBlockheightRemote(){
  global $BLOCKHEIGHT_REMOTE;
  $fn='data/blockheight';
  if(file_exists($fn)){
    $fp=fopen($fn,'r');
    $BLOCKHEIGHT_REMOTE=fread($fp,filesize($fn));
    fclose($fp);
  } else {
    $BLOCKHEIGHT_REMOTE=0;
  }
}

function saveBlockheightRemote(){
  global $BLOCKHEIGHT_REMOTE;
  $fn='data/blockheight';
  $fp=fopen($fn,'w+');
  fwrite($fp,$BLOCKHEIGHT_REMOTE);
  fclose($fp);
}

function loadBlocks($blockfile){
  $fn='data/blocks/'.$blockfile.'.json';
  if(file_exists($fn)){
    $fp=fopen($fn,'r');
    $Blocks=json_decode( fread($fp,filesize($fn)),true);
    fclose($fp);
  } else {
    $Blocks=Array('txs' => Array());
  }
  return $Blocks;
}

function saveBlocks($blockfile,$Blocks){
  $fn='data/blocks/'.$blockfile.'.json';
  $fp=fopen($fn,'w+');
  fwrite($fp,json_encode($Blocks));
  fclose($fp);
}

$Data = json_decode(file_get_contents('php://input'), true);
$action=$Data['action'];

if( $action == 'getblockheight' ){
  loadBlockheightRemote();
  echo $BLOCKHEIGHT_REMOTE;
  exit;
}

if( $action == 'updatemempool'){

  $f='data/mempool.json';
  $fp=fopen($f,'w+');
  fwrite($fp,json_encode($Data['updates']));
  fclose($fp);

  echo 'OK';
  exit;
}

if( $action == 'updateblocks' ){

  $Updates=$Data['updates'];
  if( !empty($Updates) ){

    loadBlockheightRemote();

    $lastblockfile='';
    $lastblockheight=0;
    foreach($Updates as $block){
      if( $BLOCKHEIGHT_REMOTE < $block['height'] ){
        if( $lastblockfile != $block['blockfile'] ){
          if( count($Blocks) > 0 ){
            saveBlocks($lastblockfile,$Blocks);
            $BLOCKHEIGHT_REMOTE=$lastblockheight;
            saveBlockheightRemote();
            $Blocks=Array('txs' => Array());
          }
          $lastblockfile=$block['blockfile'];
          $Blocks=loadBlocks($block['blockfile']);
        }
        $Blocks[(string)$block['height']] = $block['block'];
        foreach( $block['tx'] as $txid=>$Tx ){
          $Blocks['txs'][$txid]=$Tx;
        }
        $lastblockheight=$block['height'];
      }
    }
    if($lastblockfile!=''){
      saveBlocks($lastblockfile,$Blocks);
      $BLOCKHEIGHT_REMOTE=$lastblockheight;
      saveBlockheightRemote();
    }
  }
  echo 'OK';
  exit;
}

/*
$fp=fopen('data/mempool.txt','w+');
fwrite($fp,json_encode($Updates));
fclose($fp);

$fp=fopen('data/blocks.txt','w+');
fwrite($fp,json_encode($Updates));
fclose($fp);
*/



?>
