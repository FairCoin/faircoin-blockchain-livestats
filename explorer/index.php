<!DOCTYPE html>
<html lang="de">
  <head>
    <title>FairCoin Blockchain Live Monitor and Statistics
    </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">
    <link href="assets/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/styles.css?<?=filemtime('assets/css/styles.css');?>" rel="stylesheet">
  </head>
  <body>

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          ...
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  <button id="singlemode" class="btn btn-sm">singlemode</button>
  <h5 id="versioninfo"><a href="https://git.fairkom.net/FairCoin/faircoin-blockchain-livestats/blob/master/README.md">V1.0-beta</a></h5>
  <div class='container-fluid'>
    <div class='row'>
      <div class='col'>
        <h2>FairCoin Blockchain Live Monitor</h2>
        <div id="newsticker"></div>
      </div>
    </div>
    <div id='transactions'></div>
    <div class='row livestats'>
      <div id='tx24h' class='col'>
      </div>
      <div id='am24h' class='col'>
      </div>
    </div>
  </div>

  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="assets/js/json.js"></script>
  <script src="assets/js/script.js"></script>
  
  </body>
</html>
