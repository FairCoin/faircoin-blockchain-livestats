<?php

$blockheight=$_GET['blockheight'];
if( empty($blockheight ) ) exit;

$blockfile=intVal($blockheight/1000)*1000;
$f='data/blocks/'.$blockfile.'.json';
$fp=fopen($f,'r');
$Blocks=json_decode(fread($fp,filesize($f)),true);
fclose($fp);

if( !empty($Blocks[(string)$blockheight]) ){
  $Tx=Array();
  foreach($Blocks[(string)$blockheight]['tx'] as $i=>$txid ){
    if($i > 0){
      $Tx[$txid]=$Blocks['txs'][$txid];
    }
  }
  echo json_encode( [ $Blocks[(string)$blockheight], $Tx] );
} else {
  echo '{}';
}

?>
