var SITE_URL='http://fairplayground.info/explorer/';
var LATEST_TX={};
var BLOCKHEIGHT_CURRENT=0;
var BLOCKHEIGHT_UPDATE=false;
var BLOCKHEIGHT_SELECTION=[];
var BLOCKS={};
var BLOCKHEIGHT_LOWEST=0;
var DRAGMODE=false;
var DRAGMODE_MOUSEOUT=false;
var DRAGTIMER=0;
var DRAG_X=0;
var DRAG_Y=0;
var DRAG_T=0;
var TICKER;
var EXCHANGE_PRICE_OBJ;
var MEMPOOL={};
var SINGLEMODE=0;


$( document ).ready(
  function() {

    var blockheight=getQueryVariable('block');
    SINGLEMODE=getQueryVariable('explicit') == '1';
    $('#singlemode').toggleClass('btn-outline-light', SINGLEMODE);
    $('#singlemode').toggleClass('btn-outline-dark', !SINGLEMODE);

    if( ! blockheight ){
      loadBlockheight();
      blockheight=BLOCKHEIGHT_CURRENT;
      BLOCKHEIGHT_UPDATE=true;
      setInterval(
        function(){
          updateData();
        }, 10000
      );
    } else {
      blockheight=blockheight.split(',');
      blockheight.sort().reverse();
      BLOCKHEIGHT_CURRENT=blockheight[0];
      BLOCKHEIGHT_SELECTION=blockheight;
      blockheight=blockheight[0];
    }
    loadData(blockheight);
    DRAG_T=$('#transactions').offset().top;
    TICKER=content_load('https://exchange.faircoin.co/data/ticker','json');
    var Currencies=['EUR','USD','BTC'];
    var tmp='';
    Currencies.forEach(
      function(v,i){
        //if(tmp!='') tmp+=', ';
        tmp+='<button class="btn btn-sm btn-primary prices">'+TICKER[v].last + ' <b>'+v+'</b></button>';
        }
    );
    tmp='FreeVision exchange prices <button class="btn btn-sm btn-primary prices">1 FAIR</button> = '+tmp;
    $('#newsticker').html('<p>'+tmp+'</p>');
    EXCHANGE_PRICE_OBJ=$('.prices:first');

    $('.prices').click(
      function(){
        var C=$(this).text().toString().split(' ');
        EXCHANGE_PRICE_OBJ=$(this);
        $.each( $('.fair-amount'),
          function(i,v){
            $(v).text( ( $(v).attr('alt') * C[0]).toFixed(6) + ' ' + C[1]);
          }
        );
        $.each( $('.fair-amount-livestats'),
          function(i,v){
            var factor=1000;
            var unit='k';
            var digits=2;
            switch(C[1]){
              case 'BTC':
                factor=1;
                unit='';
              break;
              case 'EUR':
              case 'USD':
                if( $(v).attr('alt') * C[0] < 1000 ){
                  digits=2;
                }
              break;
            }
            $(v).html( ( $(v).attr('alt') * C[0] / factor ).toFixed(digits) + unit+'<small>' + C[1]+'</small><sub>24h</sub>');
          }
        );
      }
    );


    $('#singlemode').click(
      function(v,i){
        $(this).toggleClass('btn-outline-dark');
        $(this).toggleClass('btn-outline-light');
        createBrowserURL();
      }
    );

    $('.block b').click(
      function(){
        $(this).parent().toggleClass('marked');
        createBrowserURL();
      }
    );

    $('.container-fluid').scroll(
      function(){
        if( SINGLEMODE == 0 ){
          if( $('#transactions').height() + $('#transactions').offset().top < 500 ){
            if( BLOCKHEIGHT_LOWEST > -1 ){
              new_lowest=BLOCKHEIGHT_LOWEST-1;
              BLOCKHEIGHT_LOWEST=-1;
              loadData(new_lowest,false);
            }
          }
        }
      }
    );
  }
);

function createBrowserURL(){
  var B=[];
  $.each( $('.block.marked b'),
    function(i,v){
      B.push($(v).text());
    }
  );
  ChangeUrl('FairCoin Blockchain Live Stats', SITE_URL + '?block='+ B.join(',') + (($('#singlemode').hasClass('btn-outline-light') && B.length > 0 ) ? '&explicit=1' : '' )  );
}

function copyStringToClipboard (str) {
   // Create new element
   var el = document.createElement('textarea');
   // Set value (string to be copied)
   el.value = str;
   // Set non-editable to avoid focus and move outside of view
   el.setAttribute('readonly', '');
   el.style = {position: 'absolute', left: '-9999px'};
   document.body.appendChild(el);
   // Select text inside element
   el.select();
   // Copy text to clipboard
   document.execCommand('copy');
   // Remove temporary element
   document.body.removeChild(el);
}

function TxBlocks(blockheight,new_block=false){

  var B=BLOCKS[blockheight];
  if(B == undefined ) return '';
  var txcount=B['tx'].length-1;
  if(txcount == 0) txcount='';
  var tmp='';
  tmp+='<div class="row'+((new_block) ? ' new-block' : '')+' visible fade-in">';
  tmp+='<div class="tx-count">';
  tmp+='<p>'+txcount+'<p>';
  tmp+='</div>';

  tmp+='<div class="col col-md-2 bg-success block'+((!BLOCKHEIGHT_UPDATE && BLOCKHEIGHT_SELECTION.indexOf(blockheight.toString()) > -1 ) ? ' marked' : '') + '">';
  tmp+='<b>'+blockheight+'</b><br>';
  tmp+=JSON.stringify( new Date(B['time']*1000)).slice(1,20).replace(/T/g,' ');
  tmp+='</div>';
  tmp+='<div class="col">';
  B.tx.slice(1).forEach(
    function(v,i){
      tmp+='  <div class="row">';
      tmp+='    <div class="col txid">';
      tmp+='      <button type="button" class="btn btn-outline-info btn-sm no-cursor" title="transaction-id\n\n[click to copy!]">'+v+'</button>';
      tmp+='    </div>';
      tmp+='    <div class="col">';
      tmp+='      <div class="row">';
      BLOCKS.txs[v].vout.forEach(
        function(w,j){
          tmp+='    <div class="row addresses">';
          tmp+='        <button type="button" class="btn btn-outline-'+((w.is_change) ? 'secondary' : 'light' )+' btn-sm no-cursor" title="faircoin address\n\n[click to copy!]">'+w.addresses[0]+'</button>';
          tmp+='        <button type="button" class="btn btn-outline-'+((w.is_change) ? 'secondary' : 'light' )+' btn-sm fair-amount no-cursor" title="faircoin amount\n\n[click to copy!]" alt="'+w.value+'">'+w.value.toFixed(6)+' FAIR</button><br>';
          //tmp+='        <button type="button" class="btn btn-warning with-label">faircoin refugee funds</button>';
          //tmp+='        <label class="with-label"> Das ist eine Textnachricht!</label>';
          tmp+='    </div>';
        }
      );
      tmp+='      </div>';
      tmp+='    </div>';
      tmp+='  </div>';
    }
  );

  tmp+='</div>';
  tmp+='</div>';
  tmp+='</div>';
  return tmp;
}

function TxMempoolBlock(new_block=true){
  var tmp='';
  tmp+='<div class="row'+((new_block) ? ' new-block' : '')+' visible mempool fade-in">';
  tmp+='  <div class="tx-count">';
  tmp+='    <p><p>';
  tmp+='  </div>';
  tmp+='  <div class="col col-md-2 bg-warning">';
  tmp+='    <b>unconfirmed</b><br>';
  tmp+='  </div>';
  tmp+='  <div class="col mempool-txs">';
  tmp+='  </div>';
  tmp+='</div>';
  return tmp;
}

function TxMempool(Tx, txid, new_block=false){

  var tmp='';

  tmp+='  <div class="row'+((new_block) ? ' new-block' : '')+' fade-in tx'+txid+'" alt="'+txid+'">';
  tmp+='    <div class="col txid">';
  tmp+='      <button type="button" class="btn btn-outline-info btn-sm no-cursor" title="transaction-id\n\n[click to copy!]">'+txid+'</button>';
  tmp+=JSON.stringify( new Date(Tx.time*1000)).slice(1,20).replace(/T/g,' ');
  tmp+='    </div>';
  tmp+='    <div class="col">';
  tmp+='      <div class="row">';
  Tx.vout.forEach(
    function(w,j){
      tmp+='    <div class="row addresses">';
      tmp+='        <button type="button" class="btn btn-outline-'+((w.is_change) ? 'secondary' : 'light' )+' btn-sm no-cursor" title="faircoin address\n\n[click to copy!]">'+w.addresses[0]+'</button>';
      tmp+='        <button type="button" class="btn btn-outline-'+((w.is_change) ? 'secondary' : 'light' )+' btn-sm fair-amount no-cursor" title="faircoin amount\n\n[click to copy!]" alt="'+w.value+'">'+w.value.toFixed(6)+' FAIR</button><br>';
      tmp+='    </div>';
    }
  );
  tmp+='      </div>';
  tmp+='    </div>';
  tmp+='  </div>';
  tmp+='</div>';
  tmp+='</div>';
  return tmp;
}

function updateMempool(new_block=false){

  MEMPOOL=content_load('data/mempool.json','json');

  if( Object.keys(MEMPOOL).length > 0 ){
    if($('#transactions .mempool').length == 0 ){
      $('#transactions').prepend( TxMempoolBlock(new_block) );
    }
    $.each($('.mempool-tx'),
      function(i,v){
        if( MEMPOOL[ $(v).attr('alt') ] == undefined ){
          $(v).remove();
        }
      }
    );
    $.each(MEMPOOL,
      function(i,v){
        if( $('.tx'+i).length == 0 ){
          $('.mempool-txs').append( TxMempool(v,i,new_block));
        }
      }
    );
    setTimeout( function(){
        $('.new-block').toggleClass('new-block',false);
      },500
    );
    $('.mempool .tx-count > p:first').html(Object.keys(MEMPOOL).length);
  } else {
    $('#transactions .mempool').remove();
  }
}

function loadBlockheight(){
  var blockheight=content_load('data/blockheight','text');
  blockheight=parseInt(blockheight);
  BLOCKHEIGHT_CURRENT=blockheight;
}

function loadData(blockheight, reset=true ){

  if( SINGLEMODE == 0 ){

    if( BLOCKHEIGHT_CURRENT < blockheight && BLOCKHEIGHT_UPDATE ) blockheight=BLOCKHEIGHT_CURRENT;
    var blockfile=parseInt(blockheight/1000)*1000;
    var TXS=BLOCKS.txs;
    $.extend(BLOCKS, content_load('data/blocks/'+blockfile+'.json','json') );
    $.extend(BLOCKS.txs, TXS);
    if( reset ) $('#transactions').html('');

    var B=[];
    $.each(BLOCKS,function(i,v){
      B.push(i);
    });
    B.sort();

    for(var i=blockheight; i>=B[0]; i-- ){
      var tmp=TxBlocks(i);
      if( tmp == '' ) break;
      $('#transactions').append( TxBlocks(i) );
    }

  } else {

    BLOCKHEIGHT_SELECTION.forEach(
      function(v,i){
        if( BLOCKS[v] == undefined ){
          var blockfile=parseInt(v/1000)*1000;
          $.extend(BLOCKS, content_load('data/blocks/'+blockfile+'.json','json') );
        }
        $('#transactions').append( TxBlocks(v) );
      }
    );
  }

  BLOCKHEIGHT_LOWEST=i+1;

  updateMempool(false);

  if( BLOCKHEIGHT_CURRENT == blockheight && !SINGLEMODE ) calculateLiveStats(blockheight);

  $('.no-cursor').click(
    function(){
      copyStringToClipboard($(this).text().replace(/ FAIR/g,''));
    }
  );

}

function calculateLiveStats(blockheight){
    // 24h tx tx count
    var starttime=BLOCKS[blockheight].time;
    var endtime=starttime-(3600*24);

    var i=blockheight;
    var tx24h=0;
    var am24h=0;
    while(BLOCKS[i].time > endtime){
      tx24h+=BLOCKS[i].tx.length-1;
      BLOCKS[i].tx.slice(1).forEach(
        function(z,k){
          BLOCKS['txs'][z].vout.forEach(
            function(w,j){
              if(w.is_change != 1 ){
                am24h+=w.value;
              }
            }
          );
        }
      );
      i--;
      if(i < BLOCKHEIGHT_LOWEST){
        loadData(i,false);
      }
    }
    $('#tx24h').html(tx24h + '<small>tx</small><sub>24h</sub>');
    $('#am24h').html('<p class="fair-amount-livestats" alt="'+am24h.toFixed(0)+'">'+(am24h/1000).toFixed(2) + 'k<small>FAIR</small><sub>24h</sub></p>');
}

function updateData(){
  var blockheight=content_load('data/blockheight','text');
  blockheight=parseInt(blockheight);
  while( BLOCKHEIGHT_CURRENT < blockheight ){
    $('.mempool').remove();
    BLOCKHEIGHT_CURRENT++;
    var New_block=content_load('getblock.php?blockheight='+BLOCKHEIGHT_CURRENT,'json');
    BLOCKS[BLOCKHEIGHT_CURRENT.toString()]=New_block[0];
    $.extend( BLOCKS['txs'],New_block[1] );
    $('#transactions').prepend( TxBlocks(BLOCKHEIGHT_CURRENT,true) );
    setTimeout( function(){
        $('.new-block').toggleClass('new-block',false);
      },500
    );
    calculateLiveStats(BLOCKHEIGHT_CURRENT);
  }

  updateMempool(true);

  setTimeout(
    function(){
      $(EXCHANGE_PRICE_OBJ).click();
    },50
  );

}

function ChangeUrl(page, url) {
    if (typeof (history.pushState) != "undefined") {
        var obj = { Page: page, Url: url };
        history.pushState(obj, obj.Page, obj.Url);
    } else {
        alert("Browser does not support HTML5.");
    }
}
