# FairCoin Blockchain Live Monitor and Statistics

### V1.0-beta

This version is the first beta-release of the tool.
It can happen that the tool is not up-to-date all the time because of updates or network tests and improvements.
Check the timestamp of the block and you will see if the tool is up-to-date and working at this moment.

The tool starts on block 335000. The complete blockchain will added soon. 

## Features

* new blocks will updated on website automatically
* unconfirmed ( Mempool ) transactions will shown with a delay of max. 20 seconds )
* floating updated live statistics of daily count and amount of transactions
* different colors for addresses to differ between change and payment addresses.
* link to selection of blocks possible to share it


## Security advices

The blockchain data will get from faircoin client and processed by webscripts.
Bugs can cause wrong results on website. So the informations can't be guaranteed.

What means it for the user and usage of this tool?

For transactions with higher amounts it is recommend to double-check it also in the official blockchain explorer https://chain.fair.to

For medium amounts it is recommend to wait for the confirmation.

For small amounts it should be secure enough when the transaction is shown in the mempool ( unconfirmed ) on top of the explorer website.
