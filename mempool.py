# -*- coding: utf-8 -*-
import subprocess
import json
import sys
import os
import time
import urllib.request
import requests

MEMPOOL={}
REMOTE_URL='http://fairplayground.info/explorer/update.php'
# BLOCKHEIGHT=332624
BLOCKHEIGHT_INIT=333000
BLOCKHEIGHT=0
BLOCKHEIGHT_REMOTE=0
BLOCKS={}
# get link to faircoin blockchain service

fair=subprocess.check_output(['./faircoin-cli.sh']).decode('utf-8')

# get tx(JSON) by txid
def getTx(txid):
    result=subprocess.check_output([fair,'getrawtransaction',txid]).decode('utf-8').replace('\n','')
    if( len(result) > 2000 ):
        Tx={'vout' : [ { 'addresses' : ['tx_to_large'], 'value' : 0 } ] }
        return Tx
    result=subprocess.check_output([fair,'decoderawtransaction',result])
    T=json.loads(result)
    Tx={}
    Tx['vout']=[]
    for Vout in T['vout']:
        Tx['vout'].append({'value' : Vout['value'], 'n' : Vout['n'], 'addresses' : Vout['scriptPubKey']['addresses'] })
    i=-1
    ln=-1
    am=0
    for j,w in enumerate(Tx['vout']):
        x=str(round(w['value'],8)).split('.')
        if( len(x) > 0 and len(Tx['vout']) > 1 ):
            if( ( ( len(x[1]) > 5 or ln < 2 ) and am < w['value']) or am == 0 ):
                ln=len(x[1])
                i=j
                am=w['value']
    if(i > -1):
        Tx['vout'][i]['is_change']=1

    return Tx

def getBlockfile():
    global BLOCKHEIGHT
    return str(int(BLOCKHEIGHT/1000)*1000)

def getBlockfileRemote():
    global BLOCKHEIGHT_REMOTE
    return str(int(BLOCKHEIGHT_REMOTE/1000)*1000)

def loadBlocks():
    global BLOCKS
    fn='data/blocks/'+getBlockfile()+'.json'
    if( os.path.isfile(fn) ):
        file=open(fn,'r')
        BLOCKS=json.loads(file.read())
        file.close()
    else:
        BLOCKS={}
    return BLOCKS

def saveBlocks():
    global BLOCKS
    fn='data/blocks/'+getBlockfile()+'.json'
    file=open(fn,'w+')
    file.write(json.dumps(BLOCKS))
    file.close()

def postBlock(New_blocks):
    body={'action' : 'updateblocks', 'updates' : New_blocks }
    return postToRemote(body)

def loadBlockheight():
    global BLOCKHEIGHT
    fn='data/blockheight'
    if( os.path.isfile(fn) ):
        file=open(fn,'r')
        BLOCKHEIGHT=int(file.read())
        file.close()
    else:
        BLOCKHEIGHT=BLOCKHEIGHT_INIT-1

def loadBlockheightRemote():
    global BLOCKHEIGHT_REMOTE
    result=postToRemote({'action':'getblockheight'})
    BLOCKHEIGHT_REMOTE=int(result)
    if(BLOCKHEIGHT_REMOTE < BLOCKHEIGHT_INIT):
        BLOCKHEIGHT_REMOTE=BLOCKHEIGHT_INIT-1

def saveBlockheight():
    global BLOCKHEIGHT
    fn='data/blockheight'
    file=open(fn,'w+')
    file.write(str(BLOCKHEIGHT))
    file.close()

def postNewBlocks(New_blocks, New_blocks_h, force=True):
    # post blocks update to remote
    if( len(New_blocks)>=1000 or( force and len(New_blocks)>0 ) ):
        print('post new blocks: '+json.dumps(New_blocks_h))
        result=postBlock( New_blocks )
        New_blocks=[]
        New_blocks_h=[]
        if( result == 'OK' ):
            loadBlockheightRemote()
        else:
            print('Error: bad response from remote [blocks update] : '+result)
            sys.exit(0)
    return [New_blocks, New_blocks_h]


def checkBlocks():
    global BLOCKHEIGHT
    global BLOCKHEIGHT_REMOTE

    result=subprocess.check_output([fair,'getblockcount']).decode('utf-8').replace('\n','')
    blockheight=int(result)
    result=False
    New_blocks=[]
    New_blocks_h=[]

    # update remote blocks
    while(BLOCKHEIGHT_REMOTE < BLOCKHEIGHT ):
        BLOCKHEIGHT_REMOTE+=1
        B=getBlock(BLOCKHEIGHT_REMOTE)
        Tx={}
        for txid in B['tx'][1:]:
            Tx[txid]=getTx(txid)
        New_blocks.append({'blockfile' : getBlockfileRemote(), 'height' : B['height'], 'block' : getBlockdata(B), 'tx' : Tx })
        New_blocks_h.append(BLOCKHEIGHT)
        Result=True

    # update local and remote blocks
    while(blockheight > BLOCKHEIGHT):
        BLOCKHEIGHT+=1
        loadBlocks()
        B=getBlock(BLOCKHEIGHT)
        BLOCKS[str(B['height'])]=getBlockdata(B)
        print('add block '+str(B['height']))
        saveBlocks()
        saveBlockheight()
        if BLOCKHEIGHT_REMOTE < BLOCKHEIGHT:
            Tx={}
            for txid in B['tx'][1:]:
                Tx[txid]=getTx(txid)
            New_blocks.append({'blockfile' : getBlockfile(), 'height' : B['height'], 'block' : getBlockdata(B), 'tx' : Tx })
            New_blocks_h.append(BLOCKHEIGHT)
            N=postNewBlocks(New_blocks, New_blocks_h, False)
            New_blocks=N[0]
            New_blocks_h=N[1]
        result=True

    # post blocks update to remote
    postNewBlocks(New_blocks, New_blocks_h)

    return result

def getBlock(blockheight):
    result=subprocess.check_output([fair,'getblockhash',str(blockheight)]).decode('utf-8').replace('\n','')
    result=subprocess.check_output([fair,'getblock',result]).decode('utf-8').replace('\n','')
    return json.loads(result)

def getBlockdata(B):
    return {'hash' : B['hash'], 'size' : B['size'], 'time' : B['time'], 'tx' : B['tx'] }

def getMempool():

    global MEMPOOL

    # get latest unconfirmed transactions from mempool
    result=subprocess.check_output([fair,'getrawmempool'])
    Txid=json.loads(result)

    MEMPOOL={}
    # check new transactions in mempool

    for txid in Txid:
        MEMPOOL[txid]=getTx(txid)
        MEMPOOL[txid]['time']=int(time.time())

    return MEMPOOL

def postToRemote(body):
    global REMOTE_URL
    req = urllib.request.Request(REMOTE_URL)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    jsondata = json.dumps(body)
    jsondataasbytes = jsondata.encode('utf-8')   # needs to be bytes
    req.add_header('Content-Length', len(jsondataasbytes))
    response = urllib.request.urlopen(req, jsondataasbytes)
    return response.read().decode('utf-8')

# get local blockheight
loadBlockheight()

# get remote blockheight
loadBlockheightRemote()

print('Blockheight Local:')
print(BLOCKHEIGHT)
print()
print('Blockheight Remote:')
print(BLOCKHEIGHT_REMOTE)

blkcountdown=180

while(True):

    print('~' + str(blkcountdown) + ' seconds to next block!')

    # write new transaction
    print('post mempool updates')
    body = {'action' : 'updatemempool', 'updates': getMempool() }
    result=postToRemote(body)
    if(result != 'OK'):
        print('Error : bad response from remote [mempool update] :' +result)
        sys.exit(0)
    # subprocess.call(['curl','--request','POST','-H', 'Content-Type: application/json', '--data','{"unconfirmed_tx":'+json.dumps(UNCONFIRMED_TX)+'}', REMOTE_URL])

    file=open('data/mempool.json','w+')
    file.write(json.dumps(MEMPOOL))
    file.close()

    if(checkBlocks()):
        blkcountdown=180

    time.sleep(10)
    blkcountdown-=10
